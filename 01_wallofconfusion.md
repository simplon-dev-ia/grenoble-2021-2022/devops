![Wall of Confusion Image](01_wallofconfusion.svg)

# 🧱 Mur de la confusion

Le grand jour est arrivé : on vous a fourni le code source du prototype
d'application "Time Tracker" pour le suivi de temps des employés.

Ce que l'on sait de l'application pour le moment :

- Langage de programmation : Python
- Base de données : SQLite
- Vues implémentées :
    - Inscription utilisateur
    - Connexion utilisateur
    - Deconnexion utilisateur
    - Affichage des évènements de l'utilisateur
    - Ajout d'un évènement par l'utilisateur

Seul le code source brut est disponible (Python, HTML) : le développeur précédent
effectuait les démonstrations uniquement sur sa machine et a transféré uniquement
son code source par e-mail à son manager ...

Votre première mission est consiste à remettre sur pieds l'application
**sans modifications fonctionnelles** pour obtenir une version en ligne,
ainsi que de mettre en place les premières briques du processus DevOps pour
éviter le _mur de la confusion_ !

## 🚧 Etapes

Le consultant DevOps a défini les étapes initiales suivantes pour vous aider.

**Conseil** : prenez des notes pour garder une trace du processus mis en place !

### 📝 Mettre en place une board Kanban

Afin de suivre au mieux l'avancement des tâches dans les prochains jours,
vous devez mettre en place une board de type Kanban.

Vous pouvez utiliser des tickets, des labels et des jalons pour mieux vous
organiser. Libre à vous de vous organiser mais vous devez être en mesure
de donner une vision limpide de votre avancement à chaque instant !

**Conseil** : généralement les plateformes permettent de mentionner et lier les
tickets à d'autres entités (tickets, requêtes, commits) par leurs identifiants,
très pratique pour naviguer facilement et conserver de l'historique !

### ⚙️ Environnement d'exécution

Avant d'automatiser quoi que ce soit, vous devez être capable d'exécuter
l'application en local.

Vous devez identifier :

- L'environnement d'exécution (ex : version de Python)
- Les dépendances tierces (ex : librairies Python externes)
- Les dépendances externes (ex : bases de données, APIs)

Utilisez ensuite l'outil de votre choix pour matérialiser l'environnement
d'exécution complet (ex : gestionnaire de paquet, conteneur).

**Conseil** : utilisez les outils permettant un contrôle fin de l'environnement
d'exécution. Il est possible de les combiner pour plus de souplesse !

### ⌛ Versionnage du code

Vous devez versionner le code source de l'application et tous les fichiers
nécessaires à la collaboration.

Commencez par créer un dépôt Git et versionner le code code dans la branche
principale (_main_ ou _master_). Appliquez un _tag_ Git pour identifier la
version initial (ex : `0.1.0`) sur la branche principale.

**Conseil** : N'oubliez pas d'inclure un fichier `README.md` clair, un fichier
`CHANGELOG.md` et un fichier `.gitignore` !

### 🚀 Script de déploiement

Vous devez être capable de déployer l'application rapidement et régulièrement,
manuellement dans un premier temps.

Rédigez un simple script shell pour automatiser le déploiement et versionnez-le.

**Conseil** : ce script doit pouvoir s'exécuter sur n'importe quelle machine avec
le minimum d'hypothèses initiales !

### 🤖 Intégration continue initiale

Vous devez commencer à mettre en place une intégration continue au plus tôt pour
s'assurer de la bonne collaboration pour la suite !

Dans un premier temps, on vous demande uniquement d'automatiser les tâches suivantes :

1. Installation de l'environnement virtuel nécessaire
2. Vérification du style de code standardisé
   - ex : PEP8 Python avec `flake8`
3. Vérification de la présence de vulnérabilités dans les dépendances tierces
   - ex : dépendances Python avec `safety`

Bien entendu, si ces vérifications soulèvent des problèmes avec l'application initiale,
il faudra les résoudre.

Ce pipeline d'intégration continue de base doit s'exécuter à chaque _push_ et
sur chaque branche.

**Conseil** : si possible, utilisez un formatteur de code pour faciliter
l'application du style de code entre collaborateurs !

### 📦 Livraison continue initiale

Vous devez commencer à mettre en place une livraison continue au plus tôt pour
s'assurer de la disponibilité permanente de votre application auprès des premiers
utilisateurs.

Intégrez l'exécution de votre script de déploiement à la suite du pipeline
d'intégration continue.

Vous devez conditionner la tâche de déploiement à :

- La validation des tâches d'intégration continue précédentes
- La branche principale uniquement (_main_ ou _master_)

**Conseil** : utiliser le mécanisme des variables d'environnement de la plateforme
pour stocker et accéder aux différents secrets de déploiement !

### 🚦 Mettre en place un workflow PR / MR

Vous avez désormais votre code source versionné, une intégration continue, une
livraison continue et une board de Kanban.

Chaque nouvelle modification doit faire l'objet d'une branche dédiée ainsi que
d'une _merge request_ (ou _pull request_ selon la plateforme).

Vous devrez définir au moins une personne de l'équipe comme _relecteur_ qui décidera
ou non de merger vos modifications. Idéalement, aucun membre de l'équipe n'est censé
pouvoir pousser des modification directement sur la branche _main_.

**Conseil** : l'intégration continue précédemment mise en place donnera des informations
précieuses lors de la prise de décision de la fusion de chaque branche !
