![Bugs Image](02_bugs.svg)

# 🐛 Bugs !

Aïe aïe aïe ! Les premiers tests utilisateurs de l'application en ligne ont
soulevé des bugs présents dans le prototype initial :

- Un employé peut soumettre un nouvelle évènement avec 0 heures
- L'affichage des évènements ne semble pas toujours trié par ordre chronologique décroissant

Le manager IT veut que ces bugs soient résolus immédiatement et qu'ils ne se
reproduisent plus !

Vous avez la journée pour rétablir la situation mais le consultant DevOps est
toujours à vos côtés pour vous accompagner.

## 🚧 Etapes

Le consultant DevOps a défini les étapes suivantes pour vous aider.
Il faudra les appliquer indépendemment pour chacun des bugs découverts.

**Conseil** : prenez des notes pour garder une trace du processus mis en place !

### 🔎 Reproduction

Vous devez être capable de reproduire le bug avec la dernière révision du code
sur votre machine en local.

Tentez de comprendre la cause du bug et un scénario précis de reproduction.

**Conseil** : essayez de traduire le scénario de reproduction sous forme de code
le plus simple possible (ex : avec `curl` / `httpie` / `requests` pour une requête
HTTP).

### 🧪 Tests de non-régression

Avant de corriger le bug, il convient de mettre en place des _tests de non-régression_.

Le code source initial n'avait aucun test, ni unitaire ni fonctionnel ni d'intégration.
Vous allez devoir commencer à pallier à ce manque.

Utilisez le framework de test de votre choix et écrire 2 tests à minima :

- Un test validant le cas normal
- Un test validant le cas limite / d'erreur

À ce stade, les 2 tests doivent échouer.

Créez une branche, committez vos tests initiaux et poussez.

**Conseil** : essayez de respecter la structure "arrange / act / assert" pour
écrire vos tests, il seront lisibles, compréhensible et maintenables !

### 🤖 Mise à jour du CI

Si ce n'est pas encore fait, il faut maintenant mettre à jour l'intégration
continue pour exécuter les tests automatiquement.

Les plateformes orientées DevOps vous permettent généralement de remonter le
résultat des tests. Lisez la documentation de la plateforme pour mettre en place
ce procédé.

À ce stade, l'intégration continue doit échouer.

**Conseil** : vous pouvez également mettre en place une _couverture de code_,
permettant de connaître quelles lignes et branchements conditionnels de votre
code sont couverts par les tests !

### 🧯 Correction du bug

Appliquons le principe du "test rouge-vert" : les tests échouent ("rouge"),
vous devez corriger le code pour qu'ils passent ("vert").

Vous devez trouver la correction ayant le moins d'impact sur le reste de
l'application.

Une fois les tests validés en local, commitez et poussez pour vérifier que
l'intégration continue valide également votre correctif.

Lorsque le pipeline d'intégration continu valide les tests, créez une MR / PR
pour votre branche et liez-la au ticket correspondant.

**Conseil** : vérifiez que le résultat du pipeline CI ainsi que des tests sont
remontés dans la MR / PR !

### ✅ Revue de code

Assignez un coéquipier comme relecteur de votre MR / PR.

Il/elle est en charge de vérifier :

- L'état de l'intégration continue
- Les changements à fusionner

Si des modifications sont nécessaires, dialoguer via l'éditeur en ligne pour
garder une trace des échanges et modifications successives.

Une fois le travail satisfaisant pour les 2 parties, fusionner la branche
sur la branche principale.

**Conseil** : pour effectuer des modifications sur une MR / PR, il suffit de
continuer à commiter sur la branche !

### 💻 Vérification finale

Vérifiez que le déploiement de l'application intègre bien le correctif mis en place.

Si tel est le cas, mettre à jour le fichier `CHANGELOG.md` avec une nouvelle version
et ajouter le tag Git correspondant.

**Conseil** : il est généralement de bon usage d'utiliser un _versionnage sémantique_
pour les versions successives !

## Annexe

Après quelques heures de galère pour mettre en place les premiers tests pour
l'application Flask et après avoir consulté la [documentation officielle][flask-testing],
vous avez demandé de l'aide au consultant DevOps. Il accepte de vous aider
à mettre en place rapidement votre squelette de test !

En utilisant le framework de test `pytest`, créer un fichier de test `test_app.py`
dans un dossier `tests`. Voici le démarrage de ce module de test :

```python
import pytest

from pathlib import Path
from time_tracker.app import app, close_db, get_db, init_db

@pytest.fixture()
def setup_db():
    def delete_db():
        db_path = Path(__file__).parent.parent / "time_tracker.db"
        db_path.unlink(missing_ok=True)

    delete_db()
    with app.app_context():
        init_db()
    yield
    delete_db()

@pytest.fixture()
def db(setup_db):
    with app.app_context():
        conn = get_db()
        yield conn
        close_db(None)

@pytest.fixture()
def create_user(db):
    def wrapped(username, password):
        db.execute(
            """
            INSERT INTO users(username, password)
            VALUES (:username, :password)
            """,
            dict(username=username, password=password),
        )
        db.commit()

        user = db.execute(
            """
            SELECT * FROM users
            WHERE username = :username
            """,
            dict(username=username),
        ).fetchone()

        return user

    return wrapped

@pytest.fixture()
def client():
    return app.test_client()

def test_add_event_success(db, create_user, client):
    # arrange
    user = create_user("john", "test")
    with client.session_transaction() as sess:
        sess["user_id"] = user["id"]

    # act
    params = dict(date="2022-01-01", hours="7", comments="Work")
    response = client.post("/add_event", data=params)

    # assert
    assert response.status_code == 302
    assert response.headers["Location"] == "/"
    assert len(db.execute("SELECT * FROM events").fetchall()) == 1
```

Le consultant DevOps est tellement sympathique qu'il vous donne même un exemple
de test fonctionnel `test_add_event_success` ! Ce test montre l'utilisation des
fixtures misent en place pour vérifier le comportement normal de l'ajout d'un
évènement par un utilisateur connecté.

[flask-testing]: https://flask.palletsprojects.com/en/2.1.x/testing/ "Flask - Testing Flask Applications"
