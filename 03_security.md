![Security Image](03_security.svg)

# 🔐 Sécurité !

Aïe aïe aïe ! Des experts en sécurité internes ont trouvé des failles de sécurité
dans le prototype initial permettant une usurpation d'identité :

- La clé servant à signer le cookie de session est en dur dans le code
- Une injection SQL permet de se connecter avec n'importe quel compte utilisateur

Le manager IT veut que ces vulnérabilités soient comblées immédiatement et qu'elles
ne se reproduisent plus !

Vous avez la journée pour rétablir la situation mais le consultant DevOps est
toujours à vos côtés pour vous accompagner.

## 🚧 Etapes

Le consultant DevOps a défini les étapes suivantes pour vous aider.
Il faudra les appliquer indépendemment pour chacune des vulnérabilités découvertes.

**Conseil** : prenez des notes pour garder une trace du processus mis en place !

### Analyseur

Comme vous n'êtes pas un expert en cybersécurité, il convient dans un premier
temps de vérifier si un outil de détection de vulnérabilités permet de valider
la faille remontée.

Installez un analyseur (ex : `bandit` en Python) et vérifiez qu'il détecte bien
la faille.

Intégrez l'analyse de sécurité dans le processus d'intégration continue.

Créez une branche, commitez et poussez. Le CI doit échouer.

**Conseil** : en fonction de la plateforme utilisée, certains analyseurs sont déjà
intégrés et peuvent être combinés avec le.s votre.s !

### 🔎 Reproduction

Vous devez être capable de reproduire la faille avec la dernière révision du code
sur votre machine en local.

Tentez de comprendre la cause de la vulnérabilité et un scénario précis de reproduction
de l'attaque. Vous pouvez vous inspirer des exemples fournis par l'OWASP.

**Conseil** : essayez de traduire le scénario de reproduction sous forme de code
le plus simple possible (ex : avec `curl` / `httpie` / `requests` pour une requête
HTTP).

### 🧪 Tests de non-régression

Avant de corriger la faille, il convient de mettre en place des _tests de non-régression_.

Utilisez le framework de test déjà mis en place pour écrire 1 test validant la
défense contre la vulnérabilité. À ce stade, le test doit échouer.

Committez votre test et poussez. Le CI doit toujours échouer.

**Conseil** : Si possible, paramétrer le test pour tester plusieurs exemples
d'une même attaque !

### 🧯 Correction de la faille

Vous devez trouver la correction de la vulnérabilité ayant le moins d'impact
sur le reste de l'application.

Une fois le test validé en local, commitez et poussez pour vérifier que
l'intégration continue valide également votre correctif.

Lorsque le pipeline d'intégration continu valide le test, créez une MR / PR
pour votre branche et liez-la au ticket correspondant.

**Conseil** : vérifiez que le résultat du pipeline CI ainsi que des tests sont
remontés dans la MR / PR !

### ✅ Revue de code

Assignez un coéquipier comme relecteur de votre MR / PR.

Il/elle est en charge de vérifier :

- L'état de l'intégration continue
- Les changements à fusionner

Si des modifications sont nécessaires, dialoguer via l'éditeur en ligne pour
garder une trace des échanges et modifications successives.

Une fois le travail satisfaisant pour les 2 parties, fusionner la branche
sur la branche principale.

**Conseil** : pour effectuer des modifications sur une MR / PR, il suffit de
continuer à commiter sur la branche !

### 💻 Vérification finale

Vérifiez que le déploiement de l'application intègre bien le correctif mis en place.

Si tel est le cas, mettre à jour le fichier `CHANGELOG.md` avec une nouvelle version
et ajouter le tag Git correspondant.

**Conseil** : il est généralement de bon usage d'utiliser un _versionnage sémantique_
pour les versions successives !
