![DevOps Image](devops.svg)

# DevOps

> Introduction à la philosophie et aux pratiques DevOps

## Recherche d'information

- [ ] Qu'est-ce que le _DevOps_ ?
- [ ] Qu'est-ce que le "Mur de la Confusion" ?
- [ ] Quels problèmes sont à l'origine du DevOps ?
- [ ] Quelles sont les grandes étapes pour instaurer une culture DevOps ?
- [ ] Quels outils sont nécessaires à un processus DevOps ?
- [ ] Qu'est-ce que l'intégration continue (_CI_) ?
- [ ] Qu'est-ce que la livraison continue (_CD_) ?
- [ ] Qu'est-ce que l'infrastructure en tant que code (_IaC_) ?
- [ ] Que permet Git dans un workflow DevOps ?
- [ ] Que permet Docker dans un workflow DevOps ?
- [ ] Quel est le lien avec les méthodes Agiles ?
- [ ] Quelles plateformes en ligne permettent la mise en place d'une pratique DevOps ?

## Ressources

Pour référence et pour aller plus loin :

- Article : [Wikipedia - DevOps](https://en.wikipedia.org/wiki/DevOps)
- Article : [Azure - What is DevOps](https://docs.microsoft.com/en-us/devops/what-is-devops)
- Livre (Fiction) : [The Phoenix Project](https://itrevolution.com/the-phoenix-project/)

## Mini-projet

### Objectifs

Sur 3 jours, vous allez réaliser en groupe un mini-projet d'acculturation au
DevOps à l'aide d'une plateforme en ligne.

Vous serez répartis en 3 groupes et chaque groupe utilisera une plateforme
différente, au choix :

- GitLab
- GitHub
- Azure DevOps

Votre groupe n'est pas obligé d'utiliser toutes les fonctionnalités de la
plateforme mais devra faire une veille technologique pour connaître le périmètre
des possibilités.

### Contexte

Le manager IT de votre entreprise revient d'un séminaire sur l'organisation des
équipes techniques. Il est désormais convaincu que le futur des organisations IT
réside dans la pratique du DevOps et veut commencer tout de suite !

Vous venez justement d'hériter d'un prototype projet dont le code source a été
rapidement développé par un développeur qui n'est plus dans l'entreprise.
Votre manager estime qu'un petit projet est le bac à sable parfait pour s'initier
à de nouvelles pratiques. Vous êtes donc en charge de reprendre le prototype en
appliquant des pratiques DevOps.

À partir du code source initial fourni, vous devrez :

- Trouver comment exécuter le code
- Versionner le code
- Déployer le code en ligne
- Mettre en place des tests fonctionnels et/ou unitaires
- Mettre progressivement en place une intégration continue
- Mettre progressivement en place une livraison continue
- Utiliser un workflow de type _Merge Request_ / _Pull Request_
- Utiliser une board de _Kanban_ la gestion des tâches
- Mettre à jour l'application avec de nouvelles demandes

Vous serez accompagnés par un consultant DevOps qui vous guidera dans la mise en
place du processus la première semaine.

### Restitution

En fin de semaine, vous effectuerez une restitution avec à minima les éléments suivants :

- Une veille technologique sur la plateforme DevOps utilisée
- Une démonstration du processus DevOps implémenté
- Un retour d'expérience sur la mise en place d'une pratique DevOps
