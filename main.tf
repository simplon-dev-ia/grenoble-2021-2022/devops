terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.15.1"
    }
  }

  backend "http" {
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

locals {
  groupe1 = {
    mboulli = {
      principal_id = "abf6322e-4427-4cb9-b2bb-66b02369f24f"
    }
    gcaferra = {
      principal_id = "beee6421-90a1-4daf-8e72-6db181182643"
    }
  }
}

data "azurerm_subscription" "subscription" {
}

resource "azurerm_resource_group" "g1" {
  name     = "projet-devops-groupe-1"
  location = "France Central"
}

resource "azurerm_role_assignment" "rg_role_assignment" {
  for_each = local.groupe1

  scope                = azurerm_resource_group.g1.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}
